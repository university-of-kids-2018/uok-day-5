package com.ocado.uok.robot.result;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ocado.uok.robot.time.TimeRecord;

import java.text.SimpleDateFormat;
import java.util.List;

public class TimeRecordAdapter extends RecyclerView.Adapter<TimeRecordAdapter.TimeRecordViewHolder> {


    private List<TimeRecord> records;
    private final int layoutId;
    private final int durationTextId;
    private final int startTimeTextId;
    private final int positionTextId;

    public TimeRecordAdapter(List<TimeRecord> records, int layoutId, int durationTextId, int startTimeTextId, int positionTextId) {
        this.records = records;
        this.layoutId = layoutId;
        this.durationTextId = durationTextId;
        this.startTimeTextId = startTimeTextId;
        this.positionTextId = positionTextId;
    }

    @NonNull
    @Override
    public TimeRecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
        return new TimeRecordViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeRecordViewHolder viewHolder, int i) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        viewHolder.startTimeView.setText(dateFormat.format(records.get(i).getStartTimestamp()));
        viewHolder.durationView.setText(records.get(i).getReadableDuration());
        viewHolder.positionTextView.setText(i+1 + ".");
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public class TimeRecordViewHolder extends RecyclerView.ViewHolder {

        private TextView durationView;
        private TextView startTimeView;
        private TextView positionTextView;

        public TimeRecordViewHolder(@NonNull View itemView) {
            super(itemView);
            this.durationView = itemView.findViewById(durationTextId);
            this.startTimeView = itemView.findViewById(startTimeTextId);
            this.positionTextView = itemView.findViewById(positionTextId);
        }
    }
}
