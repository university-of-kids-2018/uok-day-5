package com.ocado.uok.robot.battery;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ocado.uok.robot.R;

public class BatteryFragment extends Fragment {

    private ProgressBar batteryLvlProgressBar;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Battery batteryLevelEmitter = ViewModelProviders.of(getActivity()).get(Battery.class);
        batteryLevelEmitter
                .getPower()
                .observe(this, batteryLvlProgressBar::setProgress);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.battery_fragment, container, false);
        batteryLvlProgressBar = view.findViewById(R.id.batteryLevelPB);
        return view;
    }
}
